<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class AmountStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of stock in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $txt = "This is the List of Products That are Low On Stock://";
        $products = Product::all()->where('stock','<','3');
        if(count($products)>0){
        
        if(count($products)>0){
        foreach ($products as $prod){
            $txt .= ' Product: ' . $prod->name."";
            $txt .= ' Stock: ' . $prod->stock."";
            $txt .= ' Sku: ' . $prod->sku."/";
        $to_name = 'Josue';
        $to_email = 'josue.martinez.mc@gmail.com';
        $data = array('name'=>"Admin", "body" => $txt);
    
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
            ->subject('Artisans Web Testing Mail');
        $message->from('josue.martinez.mc@gmail.com','Lazy Cat');
    });
        }
        
    }
}}}
