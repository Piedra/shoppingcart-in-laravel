<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Bill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartsController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = auth()->id();
        $carts= DB::table('carts')
        ->join('products', 'carts.product_id', '=', 'products.id')
        ->join('users', 'carts.owner_id', '=','users.id')
        ->whereRaw('users.id ='.$id)
        ->select('carts.id','products.name','products.description','products.price','carts.amount','carts.total')
            ->get();
        return view('carts.index',compact('carts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = array();
        $names = '';
        $id = auth()->id();
        $outOfStock = Cart::select('carts.id','products.name','products.stock')
        ->join('products','carts.product_id','=','products.id')
        ->whereRaw('carts.amount > products.stock')->where('owner_id','=',auth()->id())->get();
        $oldCarts= DB::table('carts')
        ->join('products', 'carts.product_id', '=', 'products.id')
        ->join('users', 'carts.owner_id', '=','users.id')
        ->whereRaw('users.id ='.$id)
        ->select('carts.id','products.id as prod','products.name','products.description','products.price','carts.amount','carts.total')
            ->get();
        foreach ($outOfStock as $stock) {
            array_push($items,$stock->id);
            $names .= '/' .$stock->name . ' Actual Stock => ' . $stock->stock;
        }
       if(count($items) > 0){
        return redirect()->back()->with('alert', 'Im Sorry, We can\'t allow this transaction 
        because the fallowing products have no longer that amount in stock:' .$names.'/Please Remove It or edit it ' ); 
       }
       foreach ($oldCarts as $cart) {
            Bill::insert(['owner_id'=>auth()->id(),'total'=>$cart->total,
            'amount'=>$cart->amount,'name'=>$cart->name,'price'=>$cart->price]);
            Cart::where('id', '=', $cart->id)->delete();
            $prod = Product::find($cart->prod);
            $prod->stock -= $cart->amount;
            $prod->save();
        }
        return redirect()->back()->with('success', 'You\'re transaction has gone Well, And you\' bill it\'s already on history ' ); 
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stock =  Product::select('stock')->where('id','=',request('product_id'))->get();
        $amount = (int)$request->get('amount');
        if($amount>$stock[0]['stock'])
        {
            return redirect()->back()->with('alert', 'Im Sorry, Im Afraid that we only have '. $stock[0]['stock'] .' on Stock');
        }
        $var = Cart::select('product_id')
        ->where('owner_id' , '=' , auth()->id())
        ->where('product_id','=',request('product_id'))
        ->get();
        if (count($var)>0) 
        {
            return redirect()->back()->with('alert', 'This Product is Already in your Cart!');
        }
        $data = request(['owner_id','product_id','amount','total']);
        $total = $data['total'] * $data['amount'];
        $data['total'] = $total;
        Cart::create($data);
       //validations
        return redirect('/carts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        $id = auth()->id();
        $prod = DB::table('carts')
        ->join('products', 'carts.product_id', '=', 'products.id')
        ->join('users', 'carts.owner_id', '=','users.id')
        ->where('users.id', '=' ,$id)
        ->where('carts.id','=',$cart->id)
        ->select('carts.id','products.name','products.description','products.price','carts.amount','carts.total')
        ->get();
        return view('carts.edit',compact('prod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Cart $cart)
    {
        $id_prod = Cart::select('product_id')->where('id','=',$cart->id)->get();
        $price = Product::select('price')->where('id','=',$id_prod[0]->product_id)->get();
        $stock =  Product::select('stock')->where('id','=',$id_prod[0]->product_id)->get();
        $amount = (int)$request->get('new_amount');
        if($amount>$stock[0]['stock'])
        {
            return redirect()->back()->with('alert', 'Im Sorry, Im Afraid that we only have '. $stock[0]['stock'] .' on Stock');
        }
        $cart->amount = $amount;
        $cart->total = $amount * $price[0]->price;
        $cart->save();
        return redirect('/carts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        $cart->delete();
        
        return redirect('/carts');
        
    }
}
