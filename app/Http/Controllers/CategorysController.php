<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CategoryRequest;

class CategorysController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('checkRole');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categorys = Category::Search($request->name)->orderBy('id',  'ASC')->paginate(2);  
        return view('categorys.index',compact('categorys'))->with('categorys', $categorys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('categorys.create') -> with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = new Category($request->all());
        $category->parent = $category->parent ?? 0;
        $category->save();
        return redirect('/categorys');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::orderBy('name', 'ASC')->where('id','!=', $category->id)->pluck('name', 'id');
        return view('categorys.edit',compact('category')) -> with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Category $category)
    {
        $category->update(request(['name', 0]));
        return redirect('/categorys');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $products = Product::all();
        $list = array();

        //echo($products. "---------". $category->id);
        foreach ($products as $value) {
           if($value->category == $category->id){
               array_push($list,$value->id);
           }
        }
        if(count($list) > 0){
            return redirect()->back()->with('alert', 'You can\'t delete this category, There are prodcuts that belongs to them');
        }
        $category->delete();
        return redirect('/categorys');
        
    }

    public function treeView(){       
        $Categorys = Category::where('parent', '=', 0)->get();
        $tree='<ul id="browser" class="filetree"><li class="tree-view"></li>';
        foreach ($Categorys as $Category) {
             $tree .='<li class="tree-view closed"<a class="tree-name">'.$Category->name.'</a>';
             foreach ($Category->childs as $key) {
                 $route = "products/".$key->id;
                $tree .= "<ul>";
                $tree .='<li class="tree-view"><a href="'.$route.'" class="tree-name">'.$key->name.'</a>';                                 
                $tree .="</li>";
                $tree .= "</ul>";

             }
            
        }
        // return $tree;
        return view('Catalog.catalog',compact('tree'));
    }
}