<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductsRequest;
use App\Http\Controllers\CategorysController;
use Image;

class EditController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('checkRole');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::Search($request->name)->orderBy('id',  'ASC')->paginate(2);  
        return view('products.main',compact('products'))-> with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('products.create') -> with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        //Image manipulation
        $name = 'noimg.png';

        $product = new Product($request->all());


        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/'.$name);
            Image::make($image)->save($location);
            $product->image = $name;
        }

        $product->image = $name;
        $product->category = $product->category ?? 0;
        $product->save();
        $products = Product::all(); 
        return redirect('/edits');
        //return view('products.main',compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function category(Request $request)
    {
        
        dd('hola');
        
    }
}
