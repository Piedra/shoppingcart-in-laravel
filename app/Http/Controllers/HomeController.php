<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Bill;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isAdmin){
            $clients = User::all()->count(); 
            $prodSold = Bill::all()->sum('amount');
            $prod = Bill::all()->sum('total');
            return view('home',compact('clients','prodSold','prod'));
        }
        $products = Bill::all()->where('owner_id','=',auth()->id())->sum('amount');
        $amount = Bill::all()->where('owner_id','2')->sum('total');
        return view('home',compact('products','amount'));

    }
}
