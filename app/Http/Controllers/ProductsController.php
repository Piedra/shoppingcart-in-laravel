<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\Category;
use App\Http\Controllers\CategorysController;
use Image;

class ProductsController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('checkRole')->except(['index','show']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Category::all();
        $products = Product::all()->where('stock','>',0);  
        return view('products.index',compact('products','categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $products = Product::orderBy('name', 'ASC')->where('id','=', $product->id)->first();
        session(['temp_img' => $product->image]);
        return view('products.edit',compact('products')) -> with('products', $products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {

        if ($this->validate_sku($request->sku, $product->id) == "") {
            $product->update(request(['sku', 'name', 'description', 'category', 'stock', 'price', 'image']));
            $name = session('temp_img');

            if ($request->hasFile('image'))
            {
                $image = $request->file('image');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $location = public_path('images/'.$name);
                Image::make($image)->save($location);
                $product->image = $name;
            }
            //dd($this->validate_sku($product->sku, $product->id));
            $product->update(request([$request->category, $request->stock, $product->image]));
            return redirect('/edits');
        }
        return redirect()->back()->with('alert', $this->validate_sku($request->sku, $product->id));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/edits');
    }

    public function validate_sku($product_sku, $id)
    {
      $res = "";
      $result = Product::all()->where('id','!=', $id);
            
      foreach ($result as $value) {  
          //echo($product_sku); 
        if ($product_sku == $value->sku) {
            $res = "This code '".$product_sku. "' already exists, please enter another";
        }

      }
      return $res;
    }
}
