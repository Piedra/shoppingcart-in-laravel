$(document).ready(function() {
    function params() {
        return {
            id: $("#id").val()
        };
    }

    function ajaxRequest() {
        return $.getJSON("http://shoppingcart.com/bills/create");
    }

    function ajaxRequestDos(date) {
        return $.getJSON("http://shoppingcart.com/data", {
            date: date
        });
    }

    function dibujarTabla() {
        var tabla = $("#tabla_ajax");
        tabla.find("tr:gt(0)").remove();
        ajaxRequest().then(function(json) {
            json.forEach(function(item) {
                tabla.append(`<tbody>
              <tr>
                  <td>${item.date}</td>
                  <td>${item.total}</td>
                  <td><button id="${
                      item.date
                  }" style="width:100%" type="info" class="inform btn btn-info"><i class="fas fa-info"></i></button></td>
              </tr>
            </tbody>`);
            });
        });
    }
    $(document).on("click", ".inform", function(e) {
        var date = $(this).attr("id");
        console.log(date);
        var tabla = $("#detail_table");
        tabla.find("tr:gt(0)").remove();
        ajaxRequestDos(date).then(function(json) {
            json.forEach(function(item) {
                tabla.append(`<tbody>
                <tr>
                <td>${item.name}</td>
                <td>${item.price}</td>
                <td>${item.amount}</td>
                <td>${item.total}</td>
                </tr>
              </tbody>`);
            });
        });
    });

    dibujarTabla();
});
