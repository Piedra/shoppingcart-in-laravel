<?php $tittle='Bills'?>

@extends('layouts.app')


@section('content')
    <div class="my_containers container col-sm-12">
        <div class="row">
                <div class="col-sm-6">
                <h1 class="content_title">Bills</h1>
                        <table name="tabla_ajax" id="tabla_ajax" class="text_table_inbill table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                </div>
                <div class="col-sm-6">
                <h1 class="content_title">Details</h1>
                <table id="detail_table" class=" text_table_bill table">
                        <tr>
                            <th style="text-align: center">Name</th>
                            <th style="text-align: center">Price</th>
                            <th style="text-align: center">Amount</th>
                            <th style="text-align: center">Total</th>
                          </tr>
                      </table>
            
                </div>
        </div>
    </div> 
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bills.js') }}"></script>
    
@endsection