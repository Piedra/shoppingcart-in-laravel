<?php $tittle='Cart/Edit'?>

@extends('layouts.app')

@section('content')
@if (session('alert'))
    <div class="alert alert-warning">
        {{ session('alert') }}
    </div>
    @endif
    <div class="container my_containers2 col-sm-8">
        <h1>Editing {{$prod[0]->name}}</h1>
        <form method="POST" action="/carts/{{$prod[0]->id}}">
            @method('PATCH')
            @csrf
            <input type="hidden" name="product_id" value="{{$prod}}">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$prod[0]->name}}</td>
                        <td>{{$prod[0]->description}}</td>
                        <td><input type="number" class="form-control" min="0" 
                            name="new_amount" id="new_amount" value="{{{$prod[0]->amount}}}"></td>
                        <td>{{$prod[0]->price}}</td>
                    </tr>
                </tbody>
            </table>
            <button style="width: 100%" class="btn btn-success">Edit</button>
        </form>
    </div>
@endsection