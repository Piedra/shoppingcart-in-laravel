<?php $tittle='Cart';
$thereCero = false;
?>

@extends('layouts.app')

@section('content')
<?php $total_cart = 0;
   
?>
@if (session('alert'))
<?php $msn = session('alert') ;
    $datos = explode("/",$msn);
?>
<div class="alert alert-danger">
    @foreach ($datos as $item)
        {{$item}}
        <br>
    @endforeach
</div>
@endif
@if (session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif
@if ($thereCero)

@endif
<div class="row my_containers col-md-12">
    <div class="container col-sm-8">
            <h1 class="content_title">{{auth()->user()->name}} Cart</h1>
                <div class="content_table">
                        <table class="table text_table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Price</th>
                                        <th>Subtotal</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach ($carts as $item)
                                        <tr>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->description}}</td>
                                            <td>{{$item->amount}}</td>
                                            <td>{{$item->price}}</td>
                                            <td>{{$item->total}}</td>
                                            <form method="GET"action="carts/{{$item->id}}/edit">
                                                @csrf   
                                                @if ($item->amount <= 0)
                                                    <td><button disabled class="btn warning"><i class="fas fa-edit"></i></button></td>
                                                    <?php $thereCero=true?>
                                                @else
                                                    <td><button class="btn warning"><i class="fas fa-edit"></i></button></td>
                                                @endif
                                            </form>
                                            <form method="POST" action="carts/{{$item->id}}">
                                                @method('DELETE')
                                                @csrf
                                                <td><button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></td>
                                            </form>
                                            <?php $total_cart += $item->total?>
                                        </tr>
                                        @endforeach
                                </tbody>
                        </table>
                </div>
    </div>
    <div class="container col-sm-4">
        <h1 class="content_title">Cart Info</h1>
        <div style=";">
                <table class="table text_table_info">
                        <thead>
                            <tr>
                                <th>Sub-total $ {{$total_cart}}</th>
                            </tr>
                            <tr>
                                <th>Shipping $ 5</th>
                            </tr>
                            <tr>
                                <th>Total $ {{$total_cart+5}}</th>
                            </tr>
                        </thead>
                </table>
                <form method="GET" action="/carts/create">
                    @csrf
                <div>
                    @if ($thereCero)
                    <button disabled class="btn_pay btn btn-success" name="btn_checkout">Check Out !</button>
                    <br>
                    <div style="text-align: center"class="alert alert-danger">
                            {{'Im Sorry, It seems To be a product with a 0 Amount on the cart, please remove it to move forward'}}
                    </div>                        
                    @else
                    <button class="btn_pay btn btn-success" name="btn_checkout">Check Out !</button>
                    @endif
                </div>
                </form>
        </div>
    </div>
</div>
@endsection