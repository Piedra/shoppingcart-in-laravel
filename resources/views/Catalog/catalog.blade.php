<?php $tittle = "Catalog" ?>
@extends('layouts.app2')
@section('content')
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dynamic Treeview with jQuery, Laravel PHP Framework Example</title>
    <link rel="stylesheet" href="http://demo.expertphp.in/css/jquery.treeview.css" />
    <script src="http://demo.expertphp.in/js/jquery.js"></script>   
    <script src="http://demo.expertphp.in/js/jquery-treeview.js"></script>
    <script type="text/javascript" src="http://demo.expertphp.in/js/demo.js"></script>
</head>
<body>

    <h1 class="catalog_tittle">Catalog</h1>
<div class="catalog_container col-md-8 container">      
    {!! $tree !!}
</div> 
</body>
</html>
@endsection
