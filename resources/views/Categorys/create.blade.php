<?php $tittle='Categories/Create'?>

@extends('layouts.app')
@section('content')
   <div style="text-align:center">
        <h1>Create a New Categorys</h1>
        <div class="container">
            @if ($errors -> any())
		    	<div class="alert alert-danger">
		    		<ul>
		    			@foreach ($errors->all() as $error)
		    				<li>{{ $error }}</li>
		    			@endforeach
		    		</ul>
		    	</div>
            @endif
        </div> 
        <form method="POST" action="/categorys">
           @csrf
            <div class="container">
                <div class="form-group">
                     <label for="title" class="label">Name</label>
                
                     <div class="controll">
                         <input type="text" class="input" name="name" placeholder="Name">
                     </div>
                </div> 
                <div class="form-group">
                     <div class="controll">
                         <button type="submit" class="btn btn-primary"><i class="far fa-check-square"></i> Accept </button>
                     </div>
                </div>
            </div>  
        </form>
    </div>  
@endsection