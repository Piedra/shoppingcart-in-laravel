<?php $tittle='Categories/Edit'?>

@extends('layouts.app')

@section('content')
<form method="POST" action="/categorys/{{$category->id}}">
    @method('PATCH')
    @csrf
   <div class="container">
        <h1>Edit Category {{$category->name}}</h1>
       <div class="buttons">
            <label for="name">Name of Category</label>
            <input required name="name" id="name" type="text" class="form-control" value="{{$category->name}}">
            <div style="text-align:right">
            <button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i> Edit </button>
            </div>
       </div>
   </div>
</form>

@endsection