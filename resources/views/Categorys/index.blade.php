<?php $tittle='Categories'?>

@extends('layouts.app')

@section('content')
@if (session('alert'))
<div style="text-align:center" class="alert alert-danger">
    {{session('alert')}}
</div>
@endif
<div class="container">
    <h1>Category</h1>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Category</th>
            <th>
              <form method="GET" action="/categorys/create">
                  @csrf
                  <div style="text-align:center">
                      <button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i></button>
                  </div>
              </form> 
              <br>
              {{ Form::open(['url' => '/categorys', 'method'=>'GET', 'class'=>'navbar-form pull-right']) }}

              <div class="input-group">

                  {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Search product...','aria-describedby' => 'search'] ) !!}
                  <span class="input-group-addon" id="search">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                  </span>

              </div>

          {{ Form::close() }}    
            </th>
            @foreach ($categorys as $category)
              <div class="col-sm-4 col-md-3">
                      <div>    
                          <tr>
                              <td class="text-info">{{$category->name}} </td>
                              <td style="text-align:right">
                                  <div class="buttons">
                                          <form method="GET" action="/categorys/{{$category->id}}/edit">
                                              @csrf
                                              <button class="btn btn-warning" type="submit"><i class="fas fa-edit"></i></button>
                                          </form>
                                          <form method="POST" action="/categorys/{{$category->id}}">
                                              @method('DELETE')
                                              @csrf
                                              <button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></i></button>
                                          </form>
                                  </div>
                              </td>
                          </tr>                 
                      </div>
              </div>
            @endforeach
        </tr>
    </table> 
    <div class="text-center"> 
        {{{ $categorys->render() }}}   
    </div>  
</div>

@endsection