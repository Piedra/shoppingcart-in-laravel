<?php $tittle='Prodcuts/Create'?>
@extends('layouts.app')
@section('content')
   <div style="text-align:center">
        <h1>Create a New Products</h1>
        <div class="container">
            @if ($errors -> any())
		    	<div class="alert alert-danger">
		    		<ul>
		    			@foreach ($errors->all() as $error)
		    				<li>{{ $error }}</li>
		    			@endforeach
		    		</ul>
		    	</div>
            @endif
        </div> 
    </div>     
    {!! Form::open(['url' => '/edits', 'method'=>'POST', 'files'=>true]) !!}
        <div class="container">

            <div class="row">

                <div class="col-md-6">

                    <div class="form-group">
                        {!! Form::label('sku', 'SKU') !!}
                        {!! Form::number('sku', null, ['class'=>'form-control', 'placeholder' => 'SKU', 'min' => '0', 'size' => '1', 'required'] ) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Name Product', 'size' => '1', 'required'] ) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Description') !!}
                        {!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>5, 'placeholder' => 'Description', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('category', 'Categorys') !!}
                        {!! Form::select('category', $categories, null, ['class'=>'form-control', 'placeholder' => 'Select option', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('stock', 'Stock') !!}
                        {!! Form::number('stock', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'min' => '0', 'size' => '1', 'required'] ) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}
                        {!! Form::number('price', null, ['class'=>'form-control', 'placeholder' => 'Price', 'min' => '0', 'size' => '1', 'required'] ) !!}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::submit('Register' , ['class'=>'btn btn-primary'] )!!}
                    </div>
                    
                </div>

                <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('image', 'Image') !!}      
                            <input  class="form-control" type="file" name="image" id="file" accept="image/*"/>
                            <br>
                            <br>

                            <div class="col-md-12">
                                <?php $img = 'noimg.png'?>
                                <img id="img" src="{{asset('images/'.$img)}}" class="img-responsive center-block" width="520" height="400">
                                   
                                </div>
                             
                                <script>
                                    function filePreview(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function (e) {
                                                $('#img').attr('src', e.target.result);
                                                /*$('#uploadForm + img').remove();
                                                $('#uploadForm').after('<img src="'+e.target.result+'" width="540" height="400"/>');*/
                                            }   
                                            reader.readAsDataURL(input.files[0]);
                                        }
                                    }   
                                 
                                    $("#file").change(function () {
                                        filePreview(this);
                                    });     
                                </script>
                            </div>    
    

                        </div>
                    </div>

            </div>

        </div>   
    {!! Form::close() !!}
        
@endsection