<?php $tittle='Prodcuts'?>
@extends('layouts.app')

@section('content')
<div style="text-align:center">
     @if (session('alert'))
          <div class="alert alert-danger">
              {{ session('alert') }}
          </div>
     @endif
</div>
<form method="POST" action="/products/{{$products->id}}" files="true" enctype="multipart/form-data">
    @method('PATCH')
    @csrf
   <div class="container">
        <h1>Edit Products {{$products->name}}</h1>
        <div class="buttons">
          
          <div class="container">

               <div class="row">
   
                   <div class="col-md-6">
   
                       <div class="form-group">
                           {!! Form::label('sku', 'SKU') !!}
                           {!! Form::number('sku', $products->sku, ['class'=>'form-control', 'placeholder' => 'SKU', 'min' => '0', 'size' => '1', 'required'] ) !!}
                       </div>
   
                       <div class="form-group">
                           {!! Form::label('name', 'Name') !!}
                           {!! Form::text('name', $products->name, ['class'=>'form-control', 'placeholder' => 'Name Product', 'size' => '1', 'required'] ) !!}
                       </div>
   
                       <div class="form-group">
                           {!! Form::label('description', 'Description') !!}
                           {!! Form::textarea('description', $products->description, ['class'=>'form-control', 'rows'=>5, 'placeholder' => 'Description', 'required']) !!}
                       </div>
   
                       <div class="form-group">
                           {!! Form::label('category', 'Categorys') !!}
                           <?php $categories = \App\Category::all()->pluck('name', 'id')?>
                           {!! Form::select('category', $categories, $products->category, ['class'=>'form-control', 'placeholder' => 'Select option', 'required']) !!}
                       </div>
   
                       <div class="form-group">
                           {!! Form::label('stock', 'Stock') !!}
                           {!! Form::number('stock', $products->stock, ['class'=>'form-control', 'placeholder' => 'Stock', 'min' => '0', 'size' => '1', 'required'] ) !!}
                       </div>
   
                       <div class="form-group">
                           {!! Form::label('price', 'Price') !!}
                           {!! Form::number('price', $products->price, ['class'=>'form-control', 'placeholder' => 'Price', 'min' => '0', 'size' => '1', 'required'] ) !!}
                       </div>
                       
                       <div class="form-group">
                              <button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i> Edit </button>
                       </div>
                       
                   </div>
   
                   <div class="col-md-6">
                           <div class="form-group">
                               {!! Form::label('image', 'Image') !!}      
                               <input  class="form-control" type="file" name="image" id="file" accept="image/*"/>
                               <br>
                               <br>
   
                               <div class="col-md-12">
                                   <?php $img = $products->image?>
                                   <img id="img" src="{{asset('images/'.$img)}}" class="img-responsive center-block" width="510" height="400">
                                      
                                   </div>
                                
                                   <script>
                                       function filePreview(input) {
                                           if (input.files && input.files[0]) {
                                               var reader = new FileReader();
                                               reader.onload = function (e) {
                                                   $('#img').attr('src', e.target.result);
                                                   /*$('#uploadForm + img').remove();
                                                   $('#uploadForm').after('<img src="'+e.target.result+'" width="540" height="400"/>');*/
                                               }   
                                               reader.readAsDataURL(input.files[0]);
                                           }
                                       }   
                                    
                                       $("#file").change(function () {
                                           filePreview(this);
                                       });     
                                   </script>
                               </div>    
       
   
                           </div>
                       </div>
   
               </div>
   
           </div>  

        </div>
   </div>
</form>

@endsection