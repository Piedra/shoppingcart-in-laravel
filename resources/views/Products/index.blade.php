<?php $tittle='Prodcuts/View'?>
@extends('layouts.app')
@section('content')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/view.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/treeview.js') }}"></script>

<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-lg-12 my-3">
            <div class="pull-right">
                <div class="btn-group">
                    <button style="font-size: 1.6em;" class="btn btn-light" id="list">
                            <i class="fas fa-th-list"></i>
                    </button>
                    <button style="font-size: 1.8em;" class="btn btn-light" id="grid">
                            <i class="fas fa-grip-horizontal"></i>
                    </button>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div id="products" class="row col-sm-12 view-group">
        @foreach ($products as $product)
                    <div class="item col-xs-4 col-lg-4">
                            <div class="thumbnail card">
                                <div class="img-event">
                                    <img class="group list-group-image img-fluid" src="{{asset('images/'.$product->image)}}" alt="" />
                                </div>
                                <div class="caption card-body">
                                    <h4 class="group card-title inner list-group-item-heading">
                                        {{$product->name}}</h4>
                                    <p class="group inner list-group-item-text">
                                        {{$product->description}}</p>
                                    <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">Price: ${{$product->price}}</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <form method="GET" action="products/{{$product->id}}">
                                            <button style="width:100%"class="btn btn-success">View</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        @endforeach
        </div>
    </div>
@endsection