<?php $tittle='Prodcuts/Admin'?>
@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Product</h1>
    <table class="table table-striped table-bordered">

        <tr>
            <th>Name</th>
            <th>SkU</th>
            <th>Description</th>
            <th>Category</th>
            <th>Stock</th>
            <th>Price</th>
            <th>Image</th>
            <th>
              <form method="GET" action="/edits/create">
                  @csrf
                  <div style="text-align:center">
                      <button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i></button><br><br>
                  </div>
              </form> 
              {{ Form::open(['url' => '/edits', 'method'=>'GET', 'class'=>'navbar-form pull-right']) }}

                <div class="input-group">
  
                    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Search product...','aria-describedby' => 'search'] ) !!}
                    <span class="input-group-addon" id="search">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </span>
  
                </div>

            {{ Form::close() }}  
            </th>

            @foreach ($products as $product)
            <div class="col-sm-4 col-md-3">
                    <div>    
                        <tr>
                            <td class="text-info">{{$product->name}} </td>
                            <td class="text-info">{{$product->sku}} </td>
                            <td class="text-info">{{$product->description}} </td>

                            @foreach(\App\Category::all() as $category)
                                @if ($category->id == $product->category)
                                    <td class="text-info">{{$category->name}} </td>
                                @endif
                            @endforeach
                            @if ($product->category == 0)
                                    <td class="text-info">None</td>
                            @endif
                            <td class="text-info">{{$product->stock}} </td>
                            <td class="text-info">{{$product->price}} </td>
                            <td class="text-info"><img class="my_images" src="{{asset('images/'.$product->image)}}" alt=""></td>
                            <td style="text-align:right">
                                <div class="buttons">
                                        <form method="GET" action="/products/{{$product->id}}/edit">
                                            @csrf
                                            <button class="btn btn-warning" type="submit"><i class="fas fa-edit"></i></button>
                                        </form>
                                        <form method="POST" action="/products/{{$product->id}}">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></i></button>
                                        </form>
                                </div>
                            </td>
                        </tr>   
                    </div>
                </div>           
          @endforeach

        </tr>

    </table>
    <div class="text-center"> 
        {{ $products->render() }}
    </div>
</div>

@endsection