<?php
$star = rand(1,5);
$custumer = rand(1,20);
$cuestions = rand(1,20);
$tittle='Prodcuts'
?>
@extends('layouts.app')
@section('content')
    @if (session('alert'))
    <div class="alert alert-warning">
        {{ session('alert') }}
    </div>
    @endif
    <div style="padding-top:100px;" class="container  row col-sm-12">
            <div class="col-sm-8 col-md-6">
                <img class="my_images_show" src="{{asset('images/'.$product->image)}}" alt="">
                
            </div>
            <div class="col-sm-4">
                <h1 class="title">{{$product->name}}</h1>
                <hr>
                <div class="row" style="padding-left:15px;">
                    <div class="row col-lg-7" style="color:orange;margin-right: 1em">
                            @for ($i = 0; $i < $star; $i++)
                            <p class="star"><i class="fas fa-star"></i>
                            @endfor
                    </div>
                    <div style="padding-left:15px;" class= "row col-lg-5 text-info">
                            <p>{{$custumer}} custemer reviews</p>
                    </div>
                </div>
                <hr>
                <form method="POST" action="/carts">
                    @csrf
                    <div class="row">
                        <div class=" col-lg-6" style="">
                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                            <input type="hidden" name="total" id="total" value="{{$product->price}}">
                            <input type="hidden" name="owner_id" id="owner_id" value="{{auth()->id()}}">
                            <p class="price_before">List Pirce: {{$var = $product->price * 1.5 }}</p>
                            <p class="basic_text">Pirce: {{$product->price}}</p>
                            <p class="basic_text">You are saving: {{$var - $product->price}}</p>
                        </div>
                        <div style="padding-left:15px;" class= "col-lg-6 text-success">
                            <p>Avilable in Stock</p>
                            <div style="padding-left:15px;" class="row">
                                <p style="padding-right:1em;">Qty:</p>
                                <input style="width:50%;" class="form-control" 
                                type="number" name="amount" id="amount" min="1" value="1">
                            </div>
                            <p class="text-info">SKU: {{$product->sku}}</p>
                        </div>
                    </div>
                    <button style="height:10%; width:100%" class="button_text btn btn-success" 
                    name="go_to_cart" id="go_to_cart">Add to cart <i class="fas fa-cart-plus"></i></button>
                </form>
                <hr>
                <div>
                    <p class="text_description">{{$product->description}}</p>
                </div>
            </div>
    </div>
@endsection