<?php $tittle = "home" ?>
@extends('layouts.app')

@section('content')
<div class=" my_containers container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class=" maintxt card-header">Status Of {{auth()->user()->name}}</div>
                <div class="card-body">
                    <p class="mainSubText">Here You can see some of your personal information</p>
                </div>
            </div>
            @if (auth()->user()->isAdmin)
            <div class="card">
                <div class="maintxt card-header">Amount of Clients Registerd</div>
                <div class="card-body">
                    <p class="mainSubText">{{$clients}}</p>
                </div>
            </div>
            <div class="card">
                <div class="maintxt card-header">Amount of Products Sold.</div>
                <div class="card-body">
                    <p class="mainSubText">{{$prodSold}}</p>
                </div>
            </div>
            <div class="card">
                <div class="maintxt card-header">Total Amount of Sales.</div>
                <div class="card-body">
                    <p class="mainSubText">{{$prod}}</p>
                </div>
            </div>
            @else
            <div class="card">
                <div class="maintxt card-header">Total of Products that i bought</div>
                <div class="card-body">
                    <p class="mainSubText">{{$products}}</p>
                </div>
            </div>
            <div class="card">
                <div class="maintxt card-header">Total amount of purchases</div>
                <div class="card-body">
                    <p class="mainSubText">{{$amount}}</p>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
