<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('catalog',array('as'=>'jquery.treeview','uses'=>'CategorysController@treeView'));
Route::resource('products','ProductsController');
Route::resource('bills','BillsController');
Route::resource('edits','EditController');
Route::resource('categorys','CategorysController');
Route::resource('carts','CartsController');
Route::get('data','BillsController@getData');
Route::get('categories','CategorysController@getMeCategories');


Route::get('/home', 'HomeController@index')->name('home');
